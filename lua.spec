%global major_version 5.4
# test version is still 5.4.3
%global test_version 5.4.3
# Place rpm-macros into proper location.
%global macrosdir %(d=%{_rpmconfigdir}/macros.d; [ -d $d ] || d=%{_sysconfdir}/rpm; echo $d)

Name:           lua
Version:        5.4.3
Release:        8
Summary:        A powerful, efficient, lightweight, embeddable scripting language
License:        MIT
URL:            http://www.lua.org/
Source0:        http://www.lua.org/ftp/lua-%{version}.tar.gz
Source1:        http://www.lua.org/tests/lua-%{test_version}-tests.tar.gz
# Source2, Source100,Patch0~3 from https://koji.fedoraproject.org/koji/buildinfo?buildID=1113624
# multilib
Source2:        luaconf.h
# copied from doc/readme.html on 2019-09-11
Source3:        mit.txt
# rpm-macro
Source1000:     macros.lua
Patch0:         lua-5.4.0-beta-autotoolize.patch
Patch1:         lua-5.3.0-idsize.patch
Patch2:         lua-5.2.2-configure-linux.patch
Patch3:         lua-5.3.0-configure-compat-module.patch
Patch6000:      backport-CVE-2021-43519.patch
Patch6001:      backport-CVE-2021-44647.patch
Patch6002:      backport-CVE-2022-28805.patch
Patch6003:      backport-CVE-2022-33099.patch
Patch6004:      backport-CVE-2021-44964.patch

BuildRequires:  automake autoconf libtool readline-devel ncurses-devel

Obsoletes:      %{name}-libs < %{version}-%{release}
Provides:       lua(abi) = %{major_version}
Provides:       %{name}-libs = %{version}-%{release}

%description
Lua is a powerful, efficient, lightweight, embeddable scripting language.
It supports procedural programming, object-oriented programming,
functional programming, data-driven programming, and data description.

%package devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig
Obsoletes:      %{name}-static < %{version}-%{release}
Provides:       %{name}-static = %{version}-%{release}

%description devel
This package contains development files for %{name}.

%package_help

%prep
%setup -q -a 1
cp %{SOURCE3} .
mv src/luaconf.h src/luaconf.h.template.in
%patch0 -p1 -E -z .autoxxx
%patch1 -p1 -z .idsize
%patch2 -p1 -z .configure-linux
%patch3 -p1 -z .configure-compat-all
%patch6000 -p1
%patch6001 -p1
%patch6002 -p1
%patch6003 -p1
%patch6004 -p1

# Put proper version in configure.ac, patch0 hardcodes 5.3.0
sed -i 's|5.3.0|%{version}|g' configure.ac
autoreconf -ifv

%build
%configure --with-readline --with-compat-module
%disable_rpath
# Autotools give me a headache sometimes.
sed -i 's|@pkgdatadir@|%{_datadir}|g' src/luaconf.h.template

# hack so that only /usr/bin/lua gets linked with readline as it is the
# only one which needs this and otherwise we get License troubles
%make_build LIBS="-lm -ldl"
# only /usr/bin/lua links with readline now #luac_LDADD="liblua.la -lm -ldl"

%install
%make_install
rm $RPM_BUILD_ROOT%{_libdir}/*.la
mkdir -p %{buildroot}/%{_libdir}/lua/%{major_version}
mkdir -p %{buildroot}/%{_datadir}/lua/%{major_version}

# Rename luaconf.h to luaconf-<arch>.h to avoid file conflicts on
# multilib systems and install luaconf.h wrapper
mv %{buildroot}%{_includedir}/luaconf.h %{buildroot}%{_includedir}/luaconf-%{_arch}.h
install -p -m 644 %{SOURCE2} %{buildroot}%{_includedir}/luaconf.h

# Install rpm-macro
install -Dpm 0644 %{SOURCE1000} $RPM_BUILD_ROOT/%{macrosdir}/macros.lua

%check
cd ./lua-%{test_version}-tests/

# Removing tests that fail under mock/koji
sed -i.orig -e '
    /db.lua/d;
    /errors.lua/d;
    ' all.lua
LD_LIBRARY_PATH=$RPM_BUILD_ROOT/%{_libdir} $RPM_BUILD_ROOT/%{_bindir}/lua -e"_U=true" all.lua

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%license mit.txt
%{_bindir}/lua
%{_bindir}/luac
%dir %{_libdir}/lua
%dir %{_libdir}/lua/%{major_version}
%dir %{_datadir}/lua
%dir %{_datadir}/lua/%{major_version}
%{_libdir}/liblua-%{major_version}.so
%{_libdir}/liblua.so

%files devel
%defattr(-,root,root)
%{_includedir}/l*.h
%{_includedir}/l*.hpp
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.a
%{macrosdir}/macros.lua

%files help
%defattr(-,root,root)
%doc README doc/*.html doc/*.css doc/*.gif doc/*.png
%{_mandir}/man1/lua*.1*

%changelog
* Fri Aug 19 2022 shixuantong <shixuantong@h-partners.com> - 5.4.3-8
- fix CVE-2021-44964

* Mon Jul 18 2022 renhongxun <renhongxun@h-partners.com> - 5.4.3-7
- fix CVE-2022-33099

* Fri Apr 15 2022 shixuantong <shixuantong@h-partners.com> - 5.4.3-6
- fix CVE-2021-44647 CVE-2022-28805

* Thu Apr 14 2022 shixuantong <shixuantong@h-partners.com> - 5.4.3-5
- fix CVE-2021-43519 patch error 

* Fri Apr 01 2022 shixuantong <shixuantong@h-partners.com> - 5.4.3-4
- fix CVE-2021-43519

* Thu Jan 13 2022 shixuantong <shixuantong@huawei.com> - 5.4.3-3
- delete liblua-5.3.so file

* Thu Dec 30 2021 liudabo <liudabo1@huawei.com> - 5.4.3-2
- add liblua-5.3.so file

* Wed Dec 29 2021 liudabo <liudabo1@huawei.com> - 5.4.3-1
- update version to 5.4.3

* Sat Aug 8 2020 shenyangyang <shenyangyang4@huawei.com> - 5.3.5-6
- delete unneeded provides

* Sat Aug 8 2020 shenyangyang <shenyangyang4@huawei.com> - 5.3.5-5
- downgrade version to 5.3.5

* Sat Aug 1 2020 xinghe <xinghe1@huawei.com> - 5.4.0-1
- update version to 5.4.0

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.3.5-4
- Package rebuild

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.3.5-3
- Package init
